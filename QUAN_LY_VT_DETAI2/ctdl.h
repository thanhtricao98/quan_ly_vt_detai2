﻿#pragma one
#include <iostream>
#include <string>
#include <cstring>
#include <string.h>
using namespace std;

#define MAXNV 500
#define MAXCT_HOADON 20

//FILE CẤU TRÚC DỮ LIỆU

struct date
{
	int ngay;
	int thang;
	int nam;
};
typedef struct date DATE;

//========= ds Vật tư - cây nhị phân ===========
struct vat_tu
{
	char ma_vt[11];
	string ten_vt;
	string don_vi_tinh;
	float so_luong_ton;
};

struct NodeVT {
	vat_tu data;
	NodeVT *pLeft;
	NodeVT *pRight;
};
typedef struct NodeVT* TREE;

//========= ds chi tiết hđ - mảng tuyến tính =========
struct chi_tiet_hd
{
	char ma_vt[11];
	float so_luong;
	int don_gia;
	float VAT;
};
typedef struct chi_tiet_hd CHI_TIET_HD;

struct ds_chi_tiet_hd
{
	CHI_TIET_HD ds[MAXCT_HOADON];
	int sl = 0;
};
typedef struct ds_chi_tiet_hd DS_CHI_TIET_HD;
  
//========= ds hóa đơn - ds liên kết đơn ===========
struct hoa_don
{
	char so_hd[21];
	DATE ngay_lap_hd;
	char loai; // N hoặc X
	DS_CHI_TIET_HD danh_sach_chi_tiet_hd;

	hoa_don *pNext;
};
typedef struct hoa_don HOA_DON;

struct ds_hoa_don
{
	HOA_DON *pHead = NULL;
	int sl = 0;
};
typedef struct ds_hoa_don DS_HOA_DON;

//========= Nhân viên - mảng con trỏ ==========
struct nhan_vien
{
	string ma_nv;
	string ho;
	string ten;
	string phai;
	
	DS_HOA_DON danh_sach_hd;
};
typedef struct nhan_vien NHAN_VIEN;

struct ds_nhan_vien
{
	NHAN_VIEN *ds[MAXNV];
	int sl = 0;
};
typedef struct ds_nhan_vien DS_NHAN_VIEN;



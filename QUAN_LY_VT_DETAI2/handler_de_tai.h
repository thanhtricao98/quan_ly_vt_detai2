﻿#pragma once
#include "ctdl.h";

//FILE XỬ LÝ YÊU CẦU ĐỀ BÀI

//****** KHAI BÁO NGUYÊN MẪU HÀM ******
//===== nhập vật tư =====
NodeVT * KhoiTaoNodeVatTu();
void NhapVatTu(TREE &ds_vt);
void Them1VatTu(TREE &ds_vt, NodeVT *p);
int SoSanhChuoi(NodeVT *vt1, NodeVT *vt2);
int KiemTraTrungMaVT(TREE t, char ma[]);

//===== in danh sách vật tư =======
void ChuyenCaySangMang(TREE t, NodeVT *ds[], int &nds);
void InDanhSachVatTu(NodeVT *ds[], int &nds);

void menu()
{
	TREE ds_vt = NULL; //phải khởi tạo cây rỗng

	bool kt = true;
	while (kt) 
	{
		system("cls");
		cout << "\n 1. Them vat tu";
		cout << "\n 2. In danh sach vat tu tang dan theo ten";
		cout << "\n 0. Thoat";
		
		int luachon;
		cout << "/n Nhap lua chon: ";
		cin >> luachon;

		switch (luachon)
		{
		case 1:
		{
			NhapVatTu(ds_vt);
			break;
		}
		case 2:
		{
			NodeVT *ds[100];
			int nds = 0;
			ChuyenCaySangMang(ds_vt, ds, nds);
			InDanhSachVatTu(ds, nds);
			system("pause");
			break;
		}
		case 0:
		{
			kt = false;
			break;
		}
		}
	}
}

//======= thêm vật tư ========
void NhapVatTu(TREE &ds_vt)
{
	NodeVT *p = KhoiTaoNodeVatTu();

	cin.ignore(); //xóa bộ nhớ đệm vì phía trên có nhập 
	char ma[11];
	do
	{
		if (KiemTraTrungMaVT(ds_vt, ma) == 1)
		{
			cout << "\n Trung ma vat tu! Vui long nhap lai! ";
		}
		cout << "\n Nhap ma vat tu: ";
		gets_s(ma);
	} while (KiemTraTrungMaVT(ds_vt, ma) == 1);
	strcpy_s(p->data.ma_vt, ma);

	cout << "\n Nhap ten vat tu: ";
	getline(cin, p->data.ten_vt);
	cout << "\n Nhap don vi tinh: ";
	getline(cin, p->data.don_vi_tinh);
	cout << "\n Nhap so luong ton: ";
	cin >> p->data.so_luong_ton;

	Them1VatTu(ds_vt, p);
}

int SoSanhChuoi(NodeVT *vt1, NodeVT *vt2)
{
	return strcmp(vt1->data.ma_vt, vt2->data.ma_vt);
}

int KiemTraTrungMaVT(TREE t, char ma[])
{
	if (t == NULL)
	{
		return 0;
	}
	else
	{
		if (strcmp(t->data.ma_vt, ma) == 0)
		{
			return 1;
		}
		else if (strcmp(t->data.ma_vt, ma) < 0)
		{
			KiemTraTrungMaVT(t->pRight, ma); //neu ma_vt < ma
		}
		else
		{
			KiemTraTrungMaVT(t->pLeft, ma);
		}
	}
}

int KiemTraCay(TREE ds_vt)
{
	return (ds_vt != NULL ? 1 : 0);
}

NodeVT * KhoiTaoNodeVatTu()
{
	NodeVT *p = new NodeVT;
	p->pLeft = NULL;
	p->pRight = NULL;
	return p;
}

void Them1VatTu(TREE &ds_vt, NodeVT *p) //thêm một vật tư mới vào TREE
{	
	if (ds_vt == NULL)
	{
		ds_vt = p;
	}
	else
	{
		if (SoSanhChuoi(ds_vt, p) < 0)
		{
			Them1VatTu(ds_vt->pRight, p);
		}
		else if (SoSanhChuoi(ds_vt, p) > 0)
		{
			Them1VatTu(ds_vt->pLeft, p);
		}
	}
}

//========= in danh sách vật tư =========
/*
Để in ds vật tư TỒN KHO theo thứ tự tên vật tư tăng dần: 
	b1: copy từng phần tử của cây nhị phân ra một mảng tuyến tính con trỏ
		
	b2: sắp xếp mảng này lại 
	
	b3: xuất mảng này lên màn hình
	
	b4: giải phóng bộ nhớ (vì là mảng con trỏ tuyến tính). Dùng mảng động này
	sẽ tiêt kiệm bộ nhớ hơn, tránh bị phân mảnh bộ nhớ.
*/

void ChuyenCaySangMang(TREE t, NodeVT *ds[], int &nds)
{
	if (t != NULL)
	{
		//chuyen data qua mang con tro
		ds[nds] = new NodeVT;
		ds[nds]->data = t->data;
		nds++;
		ChuyenCaySangMang(t->pLeft, ds, nds);
		ChuyenCaySangMang(t->pRight, ds, nds);
	}
}

void InDanhSachVatTu(NodeVT *ds[], int &nds)
{
	for (int i = 0; i < nds; i++)
	{
		cout << "\t\t\n ========= VAT TU " << i + 1 << "==========";
		cout << "\n Ma vat tu: " << ds[i]->data.ma_vt;
		cout << "\n Ten vat tu: " << ds[i]->data.ten_vt;
		cout << "\n Don vi tinh: " << ds[i]->data.don_vi_tinh;
		cout << "\n So luong ton: " << ds[i]->data.so_luong_ton;
	}
}

